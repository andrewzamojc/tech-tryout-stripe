var dotenv     = require('dotenv');
dotenv.load();

var express    = require('express');
var stylus     = require('stylus');
var nib        = require('nib');
var stripe     = require("stripe")("sk_test_DvcYBPEdOO3J8NwOcnvER779");
var bodyParser = require('body-parser')

var app = express();


// Jade

app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

// Stylus

app.use(stylus.middleware({
    src: __dirname + '/stylesheets',
    dest: __dirname + '/public/stylesheets',
    compile: function (str, path) {
        return stylus(str).set('filename', path).use(nib());
    }
}));

// Public

app.use(express.static(__dirname + '/public'))

// Body Parser

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


// Routes

app.get('/', function (req, res) {
  res.render('index', {
    'title': 'Express Skeleton'
  });
});

app.post('/payment', function(req, res) {
    var stripeToken = req.body.stripeToken;

    var charge = stripe.charges.create({
        amount: 1000, // amount in cents
        currency: "cad",
        source: stripeToken,
        description: "Example charge"
    }, function(err, charge) {
        if (err && err.type === 'StripeCardError') {
            throw 'Card error';
        }
    });
});


// Server

var server = app.listen(3000, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Example app listening at http://%s:%s', host, port);
});
